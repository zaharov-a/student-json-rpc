<?php

/**
 * https://www.jsonrpc.org/specification
 * http://www.simple-is-better.org/json-rpc/transport_http.html
 * https://habr.com/ru/post/441854/
 */

namespace Ox3a\JsonRpc\Model;


class RequestInterface
{
    public $id;

    public $jsonrpc;

    public $method;

    public $params;
}
