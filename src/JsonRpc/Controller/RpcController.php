<?php


namespace Ox3a\JsonRpc\Controller;


use Exception;
use Ox3a\Annotation\Route;
use Ox3a\Common\Controller\AbstractController;
use Ox3a\Common\Service\DispatcherService;
use Ox3a\Common\Service\ErrorCatcherService;
use Ox3a\Common\Service\RouterService;
use Ox3a\Common\View\JsonRpc20View;
use Ox3a\Common\View\RawView;
use Ox3a\JsonRpc\Model\RequestInterface;
use Ox3a\JsonRpc\Model\RpcException;
use Psr\Container\ContainerInterface;
use Zend\Http\Header\Accept;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\Response;
use Zend\Router\Exception\InvalidArgumentException;
use Zend\Router\Http\Segment;
use Zend\Stdlib\Parameters;

class RpcController extends AbstractController
{
    /**
     * @var Request
     */
    protected $_requestPrototype;

    /**
     * @var ContainerInterface
     */
    protected $_container;

    /**
     * @var RouterService
     */
    protected $_routerService;

    /**
     * @var ErrorCatcherService
     */
    protected $_errorCatcherService;


    /**
     * RpcController constructor.
     * @param ContainerInterface  $_container
     * @param RouterService       $_routerService
     * @param ErrorCatcherService $_errorCatcherService
     */
    public function __construct(
        ContainerInterface $_container,
        RouterService $_routerService,
        ErrorCatcherService $_errorCatcherService
    ) {
        $this->_container           = $_container;
        $this->_routerService       = $_routerService;
        $this->_errorCatcherService = $_errorCatcherService;
    }


    /**
     * @Route("/rpc", name="jsonRpc")
     * @throws RpcException
     */
    public function rpcAction()
    {
        $queries = $this->getRequestData();

        if (($single = !is_array($queries))) {
            $queries = [$queries];
        }

        /** @var JsonRpc20View[] $result */
        $result = [];

        foreach ($queries as $query) {
            $result[] = $this->exec($query);
        }

        if ($single) {
            return $result[0];
        }

        return new RawView(
            'application/json',
            json_encode(
                array_map(
                    function ($result) {
                        return $result->getResponse();
                    },
                    $result
                )
            )
        );
    }


    /**
     * @return RequestInterface|RequestInterface[]
     * @throws RpcException
     */
    public function getRequestData()
    {
        $request = $this->getRequestPrototype();

        $data = [];

        if ($request->isPost()) {
            $queries = json_decode($request->getContent());
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new RpcException('Parse error', -32700);
            }
        } else {
            $queries = (object)(array)$request->getQuery();

            if ((isset($queries->params) && $this->getRequest()->isGet())) {
                $queries->params = json_decode($queries->params);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    throw new RpcException('Parse error', -32700);
                }
            }
        }

        if (!$queries || (!is_array($queries) && !is_object($queries))) {
            throw new RpcException('Invalid Request', -32600);
        }


        if (($single = !is_array($queries))) {
            $queries = [$queries];
        }

        foreach ($queries as $key => $query) {
            if (!is_numeric($key)) {
                throw new RpcException('Invalid Request', -32600);
            }

            if (!property_exists($query, 'params')) {
                $query->params = null;
            }
            $data[] = $query;
        }

        return $single ? $data[0] : $data;
    }


    /**
     * @param RequestInterface $query
     * @return mixed
     */
    public function exec($query)
    {
        $response = new JsonRpc20View();
        $router   = $this->_routerService->getRouter();

        if (isset($query->id)) {
            $response->setId($query->id);
        }

        if (
            !isset($query->jsonrpc) || $query->jsonrpc != '2.0' ||
            empty($query->method)
        ) {
            return $response->setError(-32600, 'Invalid Request');
        }

        if (!$router->hasRoute($query->method)) {
            return $response->setError(-32601, 'Method not found');
        }

        try {
            $request = clone $this->getRequestPrototype();

            $request->setPost(new Parameters($query->params ? ((array)$query->params) : []));
            $request->setQuery(new Parameters($query->params ? ((array)$query->params) : []));

            /** @var Segment $route */
            $route = $this->_routerService->getRouter()->getRoute($query->method);

            $path = $route->assemble($query->params ? ((array)$query->params) : []);

            $request->setRequestUri($path);
            $request->setUri($path);
            $match = $route->match($request);

            /** @var DispatcherService $dispatcher */
            $dispatcher = $this->_container->get(DispatcherService::class);

            $result = $dispatcher->setRequest($request)
                ->setResponse(new Response())
                ->setMatch($match)
                ->process();

            if (!$result instanceof JsonRpc20View) {
                throw new RpcException('Server error', -32000);
            }

            if (isset($query->id)) {
                $result->setId($query->id);
            }

            return $result;
        } catch (Exception $e) {
            $message = $e->getMessage();

            $code = $e->getCode() ?: 0;

            if ($e instanceof InvalidArgumentException) {
                $code = -32602;
            } elseif (!$code) {
                $code = is_numeric($message) ? (int)$message : -32000;
            }

            $errorData = $this->_errorCatcherService->caught($e);

            $response->setError($code, $message, $errorData);
        }

        return $response;
    }


    public function getRequestPrototype()
    {
        if (!$this->_requestPrototype) {
            $this->_requestPrototype = $this->getRequest();
            /** @var Accept $accept */
            $accept = $this->_requestPrototype->getHeader('accept');

            $accept->addMediaType('application/json');
        }

        return $this->_requestPrototype;
    }
}
