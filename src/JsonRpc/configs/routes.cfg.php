<?php

return [
    'jsonRpc' => [
        'type'    => 'segment',
        'options' => [
            'route'       => '/rpc',
            'constraints' => [],
            'defaults'    => [
                '_action'        => 'rpcAction',
                '_actionName'    => 'rpc',
                '_controller'    => Ox3a\JsonRpc\Controller\RpcController::class,
                '_controllerDir' => 'rpc',
                '_moduleName'    => 'JsonRpc',
            ],
        ],
    ],
];
